# @4geit/ngx-auth-module [![npm version](//badge.fury.io/js/@4geit%2Fngx-auth-module.svg)](//badge.fury.io/js/@4geit%2Fngx-auth-module)

---

authentication module for angular that shows a login and register pages

## Installation

1. A recommended way to install ***@4geit/ngx-auth-module*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-auth-module) package manager using the following command:

```bash
npm i @4geit/ngx-auth-module --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-auth-module
```

2. You need to import the `NgxAuthModule` class in whatever module you want in your project for instance `app.module.ts` as follows:

```js
import { NgxAuthModule } from '@4geit/ngx-auth-module';
```

And you also need to add the `NgxAuthModule` module within the `@NgModule` decorator as part of the `imports` list:

```js
@NgModule({
  // ...
  imports: [
    // ...
    NgxAuthModule,
    // ...
  ],
  // ...
})
export class AppModule { }
```
